#############################################################
#
#librtmp
#
#############################################################
LIBRTMP_VERSION = dc76f0a8461e6c8f1277eba58eae201b2dc1d06a
LIBRTMP_SOURCE = rtmpdump-$(LIBRTMP_VERSION).tar.gz
LIBRTMP_SITE_METHOD = git
LIBRTMP_SITE = git://git.ffmpeg.org/rtmpdump
LIBRTMP_INSTALL_STAGING = YES
LIBRTMP_DEPENDENCIES = openssl

define LIBRTMP_BUILD_CMDS
	sed -ie "s|prefix=/usr/local|prefix=/usr|" $(@D)/librtmp/Makefile
        $(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" AR="$(TARGET_AR)" -C $(@D)/librtmp
endef

define LIBRTMP_INSTALL_STAGING_CMDS
	$(MAKE) -C $(@D)/librtmp install DESTDIR=$(STAGING_DIR)
endef

define LIBRTMP_INSTALL_TARGET_CMDS
	install -m 644 $(@D)/librtmp/librtmp.so.1 $(TARGET_DIR)/usr/lib
endef

$(eval $(generic-package))
